FROM ruby:latest

ENV HOME /home/rails/webapp

# Install dependencies.
# This includes NodeJS, because it is used by the Ruby Uglifier Gem.
# PostgreSQL is _not_ included, because the additional complication
# of an external database manager is not needed to demo the app.
RUN apt-get update -qq && apt-get install -y build-essential nodejs

WORKDIR $HOME

# Install gems
ADD Gemfile* $HOME/
RUN bundle install

# Add the app code
ADD . $HOME

# Default command
CMD ["rails", "server", "--binding", "0.0.0.0"]
