# Dockerized Ruby on Rails Tutorial sample application

This is a [dockerized](https://www.docker.com/) version of the micropost web service in Michael Hartl's excellent [Ruby on Rails Tutorial (Rails 5)](https://www.railstutorial.org/book). It is forked from [his BitBucket repository](https://bitbucket.org/railstutorial/sample_app_4th_ed), which has all the source code for his sample web server.

In the master branch of this repository, the _only_ changes from Hartl's repo are _(1)_ the addition of `./Dockerfile`, and _(2)_ the insertion of these instructions at the beginning of his `README.md`.

The next two sections show how to get the code up and running in a Docker container on Mac OS X. Those sections are followed by Michael Hartl's original `README.md` for his source code.

## Get the application running locally

First, you'll need to make sure you have Docker and Ruby installed.

### Get Docker
If `docker -v` succeeds, you probably have all the Docker you need.
```bash
$ docker -v 
Docker version 17.12.0-ce, build c97c6d6
```
Else, see [Docker for Mac](https://www.docker.com/docker-mac).

### Get Ruby
If `ruby -v` succeeds, you're probably good to go.
```bash
$ ruby -v
ruby 2.5.0p0 (2017-12-25 revision 61468) [x86_64-darwin17]
```
If not, grab a current [Ruby interpreter](https://www.ruby-lang.org/en/documentation/installation/#homebrew).

### Get Source Code
With those prequisites in place, the next step is to clone the repository and prepare it to run. We'll assume you're putting it in `~/hartl`, but you can adjust the paths if you choose a different directory.

```bash
$ git clone https://bitbucket.org/perlmonger42/hartl_microposts_app.git ~/hartl
$ cd ~/hartl
$ bundle install --without production  # install the required gems
$ rails db:migrate  # set up the SQLite database
$ rails test        # test to make sure everything's working correctly
$ rails db:seed     # insert sample data into the database
$ rails server      # launch the micropost web server
```
Visit your new web site by loading http://localhost:3000 in a web browser. It runs on port 3000 by default, but you can change that via the `PORT` environment variable.
```bash
$ PORT=8080 rails server  # make app available at http://localhost:8080
```

You can log in using the email address `example@railstutorial.org` and the password `foobar`. (The `rails db:seed` command set up several user accounts, many sample microposts, and some following/follower relationships. See `~/hartl/db/seeds.rb` for details.)

Stop the server by typing `CTRL-C` in the terminal where you ran `rails server`.

## Get the application running in a Docker container

You create a Docker image using the `docker build INPUT_DIRECTORY` command. The build is driven by the instructions in `INPUT_DIRECTORY/Dockerfile`. Here, we use the `-t IMAGE_NAME` option to give the image a human-readable name. Without it, you'd have to reference it by the generated ID.
```bash
$ cd ~/hartl  # make sure you're where I think you are
$ docker build -t hartl_tutorial_app .  # that `.` is the INPUT_DIRECTORY
$ docker images  # list your Docker images (your image ID will be different, of course)
REPOSITORY          TAG     IMAGE ID      CREATED       SIZE
hartl_tutorial_app  latest  5944dab62e23  17 hours ago  1.06GB
```

To launch the web service in a new Docker container, based on the image you just created, use the `docker run IMAGE_NAME` command.
```bash
$ docker run --detach --rm --publish 80:3000 --name icy_pine hartl_tutorial_app
$ open http://localhost:80  # interact with the newly-spun-up web server
```
There are a lot of arguments in that `docker run` command. Here's what they mean:

Argument              | Meaning
----------------------|--------
`--detach`            | Run the container in the background, so you get a new prompt immediately.
`--rm`                | Automatically remove the container when it exits.
`--publish 80:3000`   | Host port 80 will be bound to the container's port 3000. If instead you said `--publish 3000:3000`, you'd see the application on http://localhost:3000.
`--name icy_pine`     | Without this option, Docker will assign a random [Heroku](https://github.com/usmanbashir/haikunator)-style `CONTAINER_NAME` to the new container. However, for this tutorial it's helpful to have a fixed name.
`hartl_tutorial_app`  | The `IMAGE_NAME` from your `docker build -t IMAGE_NAME .` command.

`docker ps` will list the running containers:
```bash
$ docker ps  # list the running containers; icy_pine should be among them
CONTAINER ID  IMAGE               COMMAND                 CREATED                 STATUS        PORTS                 NAMES
62b8a777d08c  hartl_tutorial_app  "rails server --bind…"  Less than a second ago  Up 3 seconds  0.0.0.0:80->3000/tcp  icy_pine
```

When you're done playing with the micropost application, shut down the container and its web service with `docker stop CONTAINER_NAME`.
```bash
$ docker stop icy_pine
```

If you want to retain server state when you stop the container, leave off the `--rm` flag on your `docker run IMAGE_NAME`. That way Docker won't delete the container after it shuts down. The container will still be available, though `docker ps` won't show it unless you add the `-a` flag.

To get the server running again, with its retained state, use `docker restart CONTAINER_NAME`. Note that it's the `CONTAINER_NAME` there, not the `IMAGE_NAME`, because you can run several containers based on the same image (though you'll want to bind their port 3000's to different host ports).

Here is an example of the run/stop/restart pattern:

```bash
$ docker run --detach --publish 8080:3000 --name icy_pine hartl_tutorial_app
163ba4373c51cb941231fa4118ba5f759753a607039434ebba781c958b1fbdb2
$ docker stop icy_pine
icy_pine
$ docker ps -a  # -a means "include non-running containers"
CONTAINER ID        IMAGE                COMMAND                  CREATED             STATUS                     PORTS               NAMES
163ba4373c51        hartl_tutorial_app   "rails server --bind…"   7 seconds ago       Exited (0) 5 seconds ago                       icy_pine
$ docker restart icy_pine
icy_pine
```

You now have a Dockerized Ruby on Rails Tutorial sample application.

--------------------------------------------------------------------------------------------

**And now, back to your _real_ host, Michael Hartl...**

The remainder of this document is the original README.md from Michael Hartl's [BitBucket repository](https://bitbucket.org/railstutorial/sample_app_4th_ed).

--------------------------------------------------------------------------------------------

# Ruby on Rails Tutorial sample application

This is the reference implementation of the sample application for the 4th edition of [*Ruby on Rails Tutorial: Learn Web Development with Rails*](http://www.railstutorial.org/) by [Michael Hartl](http://www.michaelhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/) is available jointly under the MIT License and the Beerware License. See [LICENSE.md](LICENSE.md) for details.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ cd /path/to/repos
$ git clone https://bitbucket.org/railstutorial/sample_app_4th_ed.git sample_app_reference
$ cd sample_app_reference
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

To check out the code for a particular chapter, use

```
$ git checkout chapter-branch-name
```

where you can find the branch name using

```
$ git branch -a
```

A branch called `remotes/orgin/foo-bar` can be checked out using `git checkout foo-bar`.

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).
